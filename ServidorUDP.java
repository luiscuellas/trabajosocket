package UDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;


//En esta clase he sido ALTAMENTE ayudado por IA,
//no queria mandarlo incompleto
public class ServidorUDP {
    public static void main(String[] args) {
        try {
            DatagramSocket serverSocket = new DatagramSocket(50005);
            System.out.println("Servidor iniciado. Esperando clientes...");

            while (true) {
                byte[] receiveData = new byte[1024];
                byte[] sendData = new byte[1024];

                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                serverSocket.receive(receivePacket);

                String mensaje = new String(receivePacket.getData(), 0, receivePacket.getLength());

                if (mensaje.equals("salir")) {
                    System.out.println("Cliente desconectado.");
                    continue;
                }

                InetAddress clientIP = receivePacket.getAddress();
                int clientPort = receivePacket.getPort();

                System.out.println("Operación recibida del cliente: " + mensaje);

                // Procesar la operación
                String[] partes = mensaje.split(" ");
                double resultado = calcular(partes[1], Double.parseDouble(partes[2]), Double.parseDouble(partes[3]));

                String respuesta = Double.toString(resultado);
                sendData = respuesta.getBytes();

                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, clientIP, clientPort);
                serverSocket.send(sendPacket);
            }
        } catch (IOException e) {
            System.out.println("Error en el servidor: " + e.getMessage());
        }
    }

    private static double calcular(String operador, double var1, double var2) {
        switch (operador) {
            case "+":
                return var1 + var2;
            case "-":
                return var1 - var2;
            case "*":
                return var1 * var2;
            case "/":
                if (var2 != 0)
                    return var1 / var2;
                else
                    return Double.NaN; // División por cero
            default:
                return Double.NaN; // Operador no válido
        }
    }
}