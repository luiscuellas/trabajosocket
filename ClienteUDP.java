package UDP;

import java.io.IOException;
import java.net.*;

public class ClienteUDP {
    public static void main(String[] args) {
        try {
            DatagramSocket clientSocket = new DatagramSocket();
            InetAddress serverAddr = InetAddress.getByName("localhost");

            while (true) {
                // Pedir al usuario la operación
                System.out.println("Ingrese la operación o 'salir' para cerrar la conexión:");
                String operacion = System.console().readLine();

                // Enviar la operación al servidor
                byte[] sendData = operacion.getBytes();
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, serverAddr, 50005);
                clientSocket.send(sendPacket);

                if (operacion.equals("salir")) {
                    System.out.println("Desconectando cliente.");
                    break;
                }

                // Recibir la respuesta del servidor
                byte[] receiveData = new byte[1024];
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                clientSocket.receive(receivePacket);

                String respuesta = new String(receivePacket.getData(), 0, receivePacket.getLength());
                System.out.println("Respuesta del servidor: " + respuesta);
            }

            clientSocket.close();
        } catch (IOException e) {
            System.out.println("Error en el cliente: " + e.getMessage());
        }
    }
}